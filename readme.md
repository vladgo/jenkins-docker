# Jenkins preconfigured docker image

Here is a Jenkins image with preinstalled default plugins. For access enter `admin` login with password `admin`.

```bash
docker run -t --rm -P --name jenkins registry.gitlab.com/vladgo/jenkins-docker
```