#! /bin/bash -e

extra_java_opts=( \
  '-Djenkins.install.runSetupWizard=false' \
  '-Dio.jenkins.dev.security.createAdmin=true' \
  '-Dio.jenkins.dev.security.allowRunsOnMaster=true' \
  '-Djenkins.model.Jenkins.slaveAgentPort=50000'
)
# '-Djenkins.model.Jenkins.slaveAgentPortEnforce=true'
export JAVA_OPTS="$JAVA_OPTS ${extra_java_opts[@]}"

: "${JENKINS_HOME:="/var/jenkins_home"}"
touch "${COPY_REFERENCE_FILE_LOG}" || { echo "Can not write to ${COPY_REFERENCE_FILE_LOG}. Wrong volume permissions?"; exit 1; }
echo "--- Copying files at $(date)" >> "$COPY_REFERENCE_FILE_LOG"
find /usr/share/jenkins/ref/ -type f -exec bash -c '. /usr/local/bin/jenkins-support; for arg; do copy_reference_file "$arg"; done' _ {} +

#cp -u /usr/share/jenkins/hudson.model.UpdateCenter.xml ${JENKINS_HOME}/hudson.model.UpdateCenter.xml
#mkdir -p ${JENKINS_HOME}/update-center-rootCAs
#cp -u /usr/share/jenkins/juseppe.crt ${JENKINS_HOME}/update-center-rootCAs/juseppe.crt

# if `docker run` first argument start with `--` the user is passing jenkins launcher arguments
if [[ $# -lt 1 ]] || [[ "$1" == "--"* ]]; then

  # read JAVA_OPTS and JENKINS_OPTS into arrays to avoid need for eval (and associated vulnerabilities)
  java_opts_array=()
  while IFS= read -r -d '' item; do
    java_opts_array+=( "$item" )
  done < <([[ $JAVA_OPTS ]] && xargs printf '%s\0' <<<"$JAVA_OPTS")

  jenkins_opts_array=( )
  while IFS= read -r -d '' item; do
    jenkins_opts_array+=( "$item" )
  done < <([[ $JENKINS_OPTS ]] && xargs printf '%s\0' <<<"$JENKINS_OPTS")

  exec java "${java_opts_array[@]}" -jar /usr/share/jenkins/jenkins.war "${jenkins_opts_array[@]}" "$@"
fi

# As argument is not jenkins, assume user want to run his own process, for example a `bash` shell to explore this image
exec "$@"