# Dockerfile for building jenkins image
#
FROM centos:7

ARG JENKINS_VER

ARG user=jenkins
ARG group=jenkins
ARG uid=1000
ARG gid=1000
ARG http_port=8080
ARG agent_port=50000
ARG installed_plugins="jdk-tool ace-editor ant antisamy-markup-formatter authentication-tokens authorize-project bouncycastle-api branch-api build-timeout cloudbees-folder credentials credentials-binding display-url-api docker-commons docker-workflow durable-task email-ext external-monitor-job git git-client git-server github github-api github-branch-source gradle handlebars icon-shim jackson2-api jquery-detached junit ldap mailer mapdb-api matrix-auth matrix-project momentjs pam-auth pipeline-build-step pipeline-github-lib pipeline-graph-analysis pipeline-input-step pipeline-milestone-step pipeline-model-api pipeline-model-declarative-agent pipeline-model-definition pipeline-model-extensions pipeline-rest-api pipeline-stage-step pipeline-stage-tags-metadata pipeline-stage-view plain-credentials resource-disposer role-strategy scm-api script-security ssh-credentials ssh-slaves structs subversion timestamper token-macro windows-slaves workflow-aggregator workflow-api workflow-basic-steps workflow-cps workflow-cps-global-lib workflow-durable-task-step workflow-job workflow-multibranch workflow-scm-step workflow-step-api workflow-support job-restrictions ownership locale configuration-as-code configuration-as-code-groovy command-launcher"

ARG JENKINS_URL=https://repo.jenkins-ci.org/public/org/jenkins-ci/main/jenkins-war/${JENKINS_VER}/jenkins-war-${JENKINS_VER}.war

ENV JENKINS_UC https://updates.jenkins.io
ENV JENKINS_HOME /u01/jenkins
ENV JENKINS_SLAVE_AGENT_PORT ${agent_port}
ENV COPY_REFERENCE_FILE_LOG ${JENKINS_HOME}/copy_reference_file.log
ENV CASC_JENKINS_CONFIG=/usr/share/jenkins/ref/config.yaml

USER root

COPY ./files/jenkins.sh /usr/local/bin/jenkins.sh
COPY ./files/install-plugins.sh /usr/local/bin/install-plugins.sh
COPY ./files/jenkins-support /usr/local/bin/jenkins-support
COPY ./files/config.yaml /usr/share/jenkins/ref/config.yaml

RUN set -x \
  && chmod +x /usr/local/bin/jenkins.sh /usr/local/bin/install-plugins.sh /usr/local/bin/jenkins-support \
  && mkdir -p /usr/share/jenkins/ref/plugins ${JENKINS_HOME} \
  && groupadd -g ${gid} ${group} \
  && useradd -d "${JENKINS_HOME}" -u ${uid} -g ${gid} -m -s /bin/bash ${user} \
  && curl -sSkLf ${JENKINS_URL} \
  -o /usr/share/jenkins/jenkins.war \
  && yum install -y https://github.com/krallin/tini/releases/download/v0.18.0/tini_0.18.0-amd64.rpm \
  java-11-openjdk java-11-openjdk-devel unzip git which \
  && yum clean all \
  && chown -R ${uid}:${gid} /usr/share/jenkins ${JENKINS_HOME}

USER ${user}

#COPY ./files/hudson.model.UpdateCenter.xml /usr/share/jenkins/hudson.model.UpdateCenter.xml
#COPY ./files/juseppe.crt /usr/share/jenkins/juseppe.crt
COPY ./groovy/* /usr/share/jenkins/ref/init.groovy.d/

#RUN set -x \
#  && curl -sSLf "http://10.116.157.12/juseppe/download/plugins/update-sites-manager/latest/update-sites-manager.hpi" \
#  -o /usr/share/jenkins/ref/plugins/update-sites-manager.jpi

RUN install-plugins.sh ${installed_plugins}

ENTRYPOINT ["/usr/bin/tini", "--", "/usr/local/bin/jenkins.sh"]
EXPOSE 8080 50000

VOLUME ["/tmp", "${JENKINS_HOME}"]
